Service Worker Registration module allows to register your Service Worker script.

Please note that to register a Service Worker script
your site should have a valid SSL certificate or its domain should be "localhost".

In order to register your service worker script go to
Home -> Administration -> Configuration -> Service Worker Registration Settings and
enter a relative path to your service-worker.js (from the site root), e.g. sites/default/files/service-worker.js

An example of use:
How to implement custom offline page for Drupal 8 without a single line of code:
http://komelin.com/articles/pwa-recipe-custom-offline-page-drupal-8-without-single-line-code
